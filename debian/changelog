giftrans (1.12.2-20) unstable; urgency=medium

  * QA upload.
  * Ran wrap-and-sort.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.6.2.
      - Updated Homepage field with latest Internet Archive snapshot link.
      - Updated Vcs-* fields.
  * debian/copyright:
      - Fixed Source field using the latest Internet Archive snapshot link.
      - Fixed Upstream-Contact field.
      - Updated packaging copyright years.
  * debian/gbp.conf: added minimal configuration to document the repository
    layout.
  * debian/patches/:
      - Updated all previous header information in accordance with DEP-3.
      - 31_fix_spelling_error.diff: created to fix a typo in upstream source.
      - 32_fix_sbo_giftrans.diff: created to fix a buffer overflow. Thanks to
        Kolja Grassmann <koljagrassmann@mailbox.org>. (Closes: #1002739)
  * debian/rules: included dpkg's buildtools.mk to fix FTCBFS. Thanks to Helmut
    Grohne <helmut@subdivi.de>. (Closes: #928899)
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to perform a superficial test.
  * debian/watch: bumped to version 4.

 -- David da Silva Polverari <polverari@debian.org>  Fri, 13 Oct 2023 21:33:05 +0000

giftrans (1.12.2-19) unstable; urgency=medium

  * QA upload.
  * Migrations:
      - debian/copyright to 1.0 format.
      - DH level to 9.
      - Standards-Version to 3.9.6
  * debian/clean: added to clean the upstream.
  * debian/control: adjusted long description.
  * debian/copyright: updated all information.
  * debian/rules: added GCC hardening and removed the customized CFLAGS.
  * debian/watch: added a fake site to explain about the current status of the
      original upstream homepage.

 -- Adilson dos Reis <adilsondosreis@yahoo.com.br>  Mon, 03 Aug 2015 16:46:47 -0300

giftrans (1.12.2-18) unstable; urgency=medium

  * Orphaning package.

 -- Chris Lamb <lamby@debian.org>  Sat, 08 Feb 2014 23:51:37 +0000

giftrans (1.12.2-17) unstable; urgency=low

  * Add additional credits to manpage. Thanks to M. Kolar <mk@mkolar.org>.
  * Update Vcs-{Browser,Git}.
  * Bump Standards-Version to 3.9.2.

 -- Chris Lamb <lamby@debian.org>  Wed, 07 Nov 2012 03:09:19 +0000

giftrans (1.12.2-16) unstable; urgency=low

  * New maintainer email address.
  * Bump Standards-Version to 3.9.1.
  * New git repo locations.
  * Move to "3.0 (quilt)" source package format.
  * Move to minimal dh7 debian/rules.
  * Expand long description a little.
  * Add missing comments to patches.

 -- Chris Lamb <lamby@debian.org>  Wed, 28 Jul 2010 15:21:55 -0400

giftrans (1.12.2-15) unstable; urgency=medium

  * Remove -ftrapv from CFLAGS. It was added in the previous upload as a
    purely cautionary measure and is causing GCC to segfault on alpha. The
    associated GCC bug is #490244.
  * Remove outdated debian/prerm that handles /usr/doc/giftrans.
  * Remove xfree86-common from Depends.
  * Add debian/watch file.

 -- Chris Lamb <chris@chris-lamb.co.uk>  Fri, 18 Jul 2008 02:22:02 +0100

giftrans (1.12.2-14) unstable; urgency=medium

  * New maintainer (Closes: #485058).
  * Rework packaging to use debhelper and quilt
    - Fixes bashism in debian/rules (Closes: #478385)
    - Stricter warnings when compiling
  * Move debian/copyright to the machine-parsable format
  * debian/control:
    - Add debhelper and quilt to Build-Depends
    - Add ${misc:Depends} to binary package Depends
    - Bump Standards-Version to 3.8.0
    - Add Vcs-{Git,Browser} fields

 -- Chris Lamb <chris@chris-lamb.co.uk>  Sun, 08 Jun 2008 04:08:16 +0100

giftrans (1.12.2-13) unstable; urgency=low

  * Cope with DEB_BUILD_OPTIONS=nostrip. Closes: #436984
  * Remove postinst - /usr/doc is gone
  * Update Standards-Version

 -- Steve McIntyre <93sam@debian.org>  Sun, 12 Aug 2007 22:58:58 +0100

giftrans (1.12.2-12) unstable; urgency=low

  * Update Depends: to cope with new xorg-provided x11-common. Closes: #362905
  * Updated to latest Standards-Version and fixed several lintian warnings.

 -- Steve McIntyre <93sam@debian.org>  Wed, 26 Apr 2006 23:58:58 +0100

giftrans (1.12.2-11) unstable; urgency=low

  * Add a warning to the manpage about needing to escape # characters when
    specifying colours. Closes: #270499.
  * Updated to latest Standards-Version and fixed several lintian warnings.

 -- Steve McIntyre <93sam@debian.org>  Sun, 12 Dec 2004 02:50:05 +0000

giftrans (1.12.2-10) unstable; urgency=low

  * Fix handling of gif files with local colour tables. Thanks to John
    Lightsey for the patch! Closes: 20715
  * Updated to latest Standards-Version and fixed several lintian warnings.

 -- Steve McIntyre <93sam@debian.org>  Tue, 22 Jul 2003 00:09:05 +0100

giftrans (1.12.2-9) unstable; urgency=low

  * Now depends on xfree86-common for rgb.txt instead of xserver-common. Closes: #129852

 -- Steve McIntyre <93sam@debian.org>  Fri, 18 Jan 2002 19:46:03 -0000

giftrans (1.12.2-8) unstable; urgency=low

  * Updated Standards-Version.
  * Fixed lintian warnings.
  * New Maintainer address.

 -- Steve McIntyre <93sam@debian.org>  Sun, 13 Jan 2002 01:46:03 -0000

giftrans (1.12.2-7) unstable; urgency=low

  * Now depends on xserver-common for rgb.txt. Fixes: Bug#76197
  * Now copes with comments in rgb.txt. Thanks to Dave Carrigan <dave@rudedog.org> for the patch. Fixes: Bug#76951

 -- Steve McIntyre <stevem@chiark.greenend.org.uk>  Tue,  16 May 2000 02:03:40 +0100

giftrans (1.12.2-6) unstable; urgency=low

  * Removed backup (*~) files - reduces diff size. Closes Bug#62601.
  * Fixed several warnings from lintian
  * Fixed two compilation warnings.

 -- Steve McIntyre <stevem@chiark.greenend.org.uk>  Tue,  16 May 2000 02:03:40 +0100

giftrans (1.12.2-5) frozen unstable; urgency=low

  * Fixed shlibdeps problem. Fixes Bug #52324.
  * Updated Standards-Version.

 -- Steve McIntyre <stevem@chiark.greenend.org.uk>  Sun,  13 Feb 2000 16:03:40 +0000

giftrans (1.12.2-4) unstable; urgency=low

  * New (old) maintainer. See Bug#24341 for details.

 -- Steve McIntyre <stevem@chiark.greenend.org.uk>  Sat,  11 Jul 1998 13:23:23 +0100

giftrans (1.12.2-3) unstable; urgency=low

  * Re-build, since previous uploads seem to have disappeared. No changes
    to source except this changelog entry.

 -- Lars Wirzenius <liw@iki.fi>  Wed,  8 Jul 1998 18:24:23 +0300

giftrans (1.12.2-2) unstable; urgency=low

  * Moved from /usr/X11R6 to /usr (I'd copied the script from a package
    with X programs...).
  * Compiled using glibc2.

 -- Lars Wirzenius <liw@iki.fi>  Mon, 20 Oct 1997 05:19:23 +0300

giftrans (1.12.2-1) unstable; urgency=low

  * Converted to new Debian packaging format, using new upstream version
    and Erick Branderhorst's (branderhorst@heel.fgg.eur.nl) old version.
  * Moved from section non-free to unstable/graphics, because it is GPL'd,
    and does not do any compression, so there is no need for it not to
    be in the main part of the distribution.

 -- Lars Wirzenius <liw@iki.fi>  Fri, 4 Jul 1997 22:10:05 +0300
